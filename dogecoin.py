__author__ = 'mehan_000'

import nltk.data
from nltk.tag import pos_tag
import random

TEXT = """
Obviously, not the most robust solution, but it'll do fine in most cases.
The only case this won't cover is abbreviations (perhaps run through the list of
sentences and check that each string in sentences starts with a capital letter?)
Anyone who reads Old and Middle English literary texts will be familiar with the mid-brown volumes of the EETS,
with the symbol of Alfred's jewel embossed on the front cover. Most of the works attributed to King Alfred or
to Aelfric, along with some of those by bishop Wulfstan and much anonymous prose and verse from the
pre-Conquest period, are to be found within the Society's three series; all of the surviving medieval
drama, most of the Middle English romances, much religious and secular prose and verse including the
English works of John Gower, Thomas Hoccleve and most of Caxton's prints all find their place in the
publications. Without EETS editions, study of medieval English texts would hardly be possible.
"""

TEXT = """
Hello people.
This is my firs letter to all the humans.
We are aliens from another planet.
Any you must obey us from this moment.
"""

def pre_word():
    x = ['so ', 'much ', 'such ', 'very ']
    return random.choice(x)

def dogecoin(input_text):

    tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')
    #fp = open("test.txt")
    #data = fp.read()
    data = input_text
    for sentence in tokenizer.tokenize(data):
        #print "".join((sentence, " wow "))
        tagged_words = pos_tag(sentence.split())
        #print tagged_words
        #tagged_words = ["so " + word for word,pos in tagged_words if pos == 'NN' or 'NNP']
        out = []
        for word,pos in tagged_words:
             if pos == 'NN' or pos == 'NNP':
                 out.append(pre_word() + word)
             else :
                 out.append(word)
        print " ".join(out)
    return input_text

if __name__ == "__main__" :
    dogecoin(TEXT)
